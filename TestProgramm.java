import java.util.ArrayList;
import java.util.Scanner;

import static java.lang.Thread.sleep;

public class TestProgramm {
    public  void Test(ArrayList<AnsAndQue> questions) throws InterruptedException {
        Scanner scanner = new Scanner(System.in);
        questions = additionQues(questions);
        int countOfQuest = questions.size();
        ArrayList<Integer> numbersOfAns = countOfQueInTest(questions, countOfQuest);
        int countOfQue = numbersOfAns.size();
        toBeginTest(questions);
        runningTest(countOfQue, numbersOfAns, questions);
        sleep(200);
        System.out.println("Do you wanna to close the program?");
        if (scanner.nextLine().compareToIgnoreCase("yes") == 0) {
            System.exit(0);
        }
        numbersOfAns.clear();
        Test(questions);
    }
    public ArrayList<AnsAndQue> additionQues(ArrayList<AnsAndQue> questions) throws InterruptedException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Mr. Teacher, do you wanna to add the question?");
        if (scanner.nextLine().compareToIgnoreCase("no") == 0) {
            return questions;
        }
        System.out.println("Mr. Teacher enter number of questions to the base, please");
        int countOfQuesInTest = scanner.nextInt();
        for (int i = 0; i < countOfQuesInTest; i++) {
            System.out.println("Mr. Teacher add your question to the base, please");
            scanner.nextLine();
            String question = scanner.nextLine();
            String ans1 = scanner.nextLine();
            String ans2 = scanner.nextLine();
            String ans3 = scanner.nextLine();
            AnsAndQue quest = new AnsAndQue(question, ans1, ans2, ans3);
            System.out.println("Mr. Teacher enter number of true answer, please");
            sleep(100);
            chooseTrueAns(scanner.nextInt(), quest);
            questions.add(quest);
        }
        return questions;
    }
    public ArrayList<Integer> countOfQueInTest(ArrayList<AnsAndQue> questions, int countOfQuest) throws InterruptedException {
        Scanner scanner = new Scanner(System.in);
        ArrayList<Integer> numbersOfAns = new ArrayList<>();
        System.out.println("Mr. Teacher enter number of questions for the student");
        int countOfQue = scanner.nextInt();
        if (countOfQue > countOfQuest) {
            System.out.println("Try again, the base contains less questions");
            sleep(400);
            countOfQueInTest(questions, countOfQuest);
            System.out.println("Do you want to finish teh programm?");
            String s = scanner.nextLine();
            if(s.compareToIgnoreCase("yes")==0){
                System.exit(0);
            }
        } else {
            while (numbersOfAns.size() != countOfQue) {
                int k = rnd(countOfQue + 1);
                if (!numbersOfAns.contains(k)) {
                    numbersOfAns.add(k);
                }
            }
        }
        return numbersOfAns;
    }
    public void toBeginTest(ArrayList<AnsAndQue> questions) throws InterruptedException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Type 'yes' to start the TEST");
        String s = scanner.nextLine();
        if(s.compareToIgnoreCase("yes")!=0){
            System.out.println("Do you wanna to close the program?");
            if (scanner.nextLine().compareToIgnoreCase("yes") == 0) {
                System.exit(0);
            }
            Test(questions);
        }
    }

    public void runningTest(int countOfQue, ArrayList<Integer> numbersOfAns, ArrayList<AnsAndQue> questions) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please, write your name and surname");
        String student = scanner.nextLine();
        ArrayList<AnsAndQue> wrongQuestions = new ArrayList<>();
        Results result = new Results();
        Time time = new Time();
        for (int i = 0; i < countOfQue; i++) {
            int v = numbersOfAns.get(i);
            System.out.println(questions.get(v - 1).displayQuestion());
            String studentAnswer = scanner.nextLine();
            if (compareAns(studentAnswer, questions.get(v - 1))) {
                result.setCountOfTrueAns(result.getCountOfTrueAns() + 1);
                result.setScore(result.getScore() + 2);
            } else {
                result.setScore(result.getScore() - 1);
                wrongQuestions.add(questions.get(v - 1));
            }
        }
        System.out.println("You completed the Test in " + time.getTime() + " sec");
        result.displayResult(student, wrongQuestions);
    }

    public boolean compareAns(String studentAnswer, AnsAndQue question) {
        return studentAnswer.compareToIgnoreCase(question.getTrueAnswer()) == 0;
    }
    public static void displayWrongAns(ArrayList<AnsAndQue> wrngQuestions) {
        System.out.println("Wrong answers:" + '\n');
        for (int i = 0; i < wrngQuestions.size(); i++) {
            System.out.println((i + 1) + ") " + wrngQuestions.get(i).toString());
        }
    }

    public void chooseTrueAns(int trueAns, AnsAndQue questions) {
        switch (trueAns) {
            case 1 -> questions.setTrueAnswer(questions.getAns1());
            case 2 -> questions.setTrueAnswer(questions.getAns2());
            case 3 -> questions.setTrueAnswer(questions.getAns3());
        }
    }

    public int rnd(int max) {
        return (int) (Math.random() * max) + 1;
    }
}

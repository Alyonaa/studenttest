import java.util.ArrayList;

public class Results {
    private int countOfTrueAns = 0;
    private int score = 0;

    public int getCountOfTrueAns() {
        return countOfTrueAns;
    }

    public void setCountOfTrueAns(int countOfTrueAns) {
        this.countOfTrueAns = countOfTrueAns;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public void displayResult(String Student, ArrayList<AnsAndQue> wrongAnswers) {
        System.out.println(Student + " " + "Results: " + '\n' +
                "count of true answers = " + countOfTrueAns + '\n' +
                "score = " + score + '\n');
        TestProgramm.displayWrongAns(wrongAnswers);
    }

}


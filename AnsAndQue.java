public class AnsAndQue {
    private final String question;
    private final String ans1;
    private final String ans2;
    private final String ans3;
    private String trueAnswer;

    public AnsAndQue(String question, String ans1, String ans2, String ans3) {
        this.question = question;
        this.ans1 = ans1;
        this.ans2 = ans2;
        this.ans3 = ans3;
    }

    @Override
    public String toString() {
        return question + ": " + trueAnswer;

    }

    public String displayQuestion() {
        return "question: " + " " + question + '\n' +
                "1) ans1 = " + ans1 + '\n' +
                "2) ans2 = " + ans2 + '\n' +
                "3) ans3 = " + ans3 + '\n';
    }

    public String getTrueAnswer() {
        return trueAnswer;
    }

    public void setTrueAnswer(String trueAnswer) {
        this.trueAnswer = trueAnswer;
    }


    public String getAns1() {
        return ans1;
    }

    public String getAns2() {
        return ans2;
    }

    public String getAns3() {
        return ans3;
    }
}
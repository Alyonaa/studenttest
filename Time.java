public class Time {
    private final long Milli = System.currentTimeMillis();

    public int getTime() {
        long nowMillis = System.currentTimeMillis();
        return (int) ((nowMillis - this.Milli) / 1000);
    }
}

